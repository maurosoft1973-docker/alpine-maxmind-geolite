image: docker:23.0.4

variables:
    # DOCKER_HOST
    # DOCKER_HUB_USERNAME
    # DOCKER_HUB_PASSWORD
    # GITLAB_TOKEN
    DOCKER_HOST: $DOCKER_HOST
    DOCKER_TLS_CERTDIR: ""
    DOCKER_CLI_EXPERIMENTAL: enabled
    DOCKER_DRIVER: overlay2
    DOCKER_IMAGE_TEST: registry.gitlab.com/maurosoft1973-docker/alpine-maxmind-geolite:test

include:
    - template: Jobs/Build.gitlab-ci.yml
    - template: Security/Container-Scanning.gitlab-ci.yml

services:
    - name: docker:23.0.4-dind
      alias: docker

stages:
    - download
    - test
    - scanning
    - build
    - readme
    - clean

before_script:
    - apk add --no-cache git bash curl file wget
    - /bin/bash
    - ENV_JOB=.${CI_ALPINE_VERSION:-""}
    - |
        if [ "${CI_ALPINE_VERSION:-""}" == "" ]; then 
            ENV_JOB=.env
        else
            ENV_JOB=.env.${CI_ALPINE_VERSION:-""}
        fi
    - set -a;
    - source ./${ENV_JOB};
    - set +a;

download:
    stage: download
    image: docker:23.0.4
    tags:
        - dind64
    artifacts:
        paths:
            - maxmind
            - db.mmdb
    script:
        - |
            echo "AccountID $GEOIPUPDATE_ACCOUNT_ID" >> /etc/GeoIP.conf
            echo "LicenseKey $GEOIPUPDATE_LICENSE_KEY" >> /etc/GeoIP.conf
            echo "EditionIDs $GEOIPUPDATE_EDITION_IDS" >> /etc/GeoIP.conf
            echo "PreserveFileTimes 1" >> /etc/GeoIP.conf
            wget "https://github.com/maxmind/geoipupdate/releases/download/v${MAXMIND_GEOIPUPDATE_VERSION}/geoipupdate_${MAXMIND_GEOIPUPDATE_VERSION}_linux_amd64.tar.gz" -O ./geoipupdate_${MAXMIND_GEOIPUPDATE_VERSION}_linux_amd64.tar.gz
            tar zxvf ./geoipupdate_${MAXMIND_GEOIPUPDATE_VERSION}_linux_amd64.tar.gz
            chmod +x ./geoipupdate_${MAXMIND_GEOIPUPDATE_VERSION}_linux_amd64/geoipupdate
            ./geoipupdate_${MAXMIND_GEOIPUPDATE_VERSION}_linux_amd64/geoipupdate -f /etc/GeoIP.conf -d /tmp
            MAXMIND_GEOLITE_VERSION=$(date -r /tmp/GeoLite2-City.mmdb +"%Y%m%d")
            MAXMIND_GEOLITE_VERSION_DATE=$(date -r /tmp/GeoLite2-City.mmdb +"%Y-%m-%d")
            echo "MAXMIND_GEOLITE_VERSION=$MAXMIND_GEOLITE_VERSION" >> ./maxmind
            echo "MAXMIND_GEOLITE_VERSION_DATE=$MAXMIND_GEOLITE_VERSION_DATE" >> ./maxmind
            cp -p /tmp/GeoLite2-City.mmdb db.mmdb

build:
    stage: test
    image: docker:23.0.4
    tags:
        - dind64
    dependencies:
        - download
    script:
        - |
            source maxmind
            cp db.mmdb $CI_PROJECT_DIR/src/db.mmdb
        - BUILD_DATE=$(date +"%Y-%m-%d")
        - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
        - |
            while IFS=" " read -r JOB_ARCH
            do
                PLATFORM="linux/amd64"
                if [ "${JOB_ARCH}" == "aarch64" ]; then
                    PLATFORM="linux/arm64"
                elif [ "${JOB_ARCH}" == "armhf" ]; then
                    PLATFORM="linux/arm/v6"
                elif [ "${JOB_ARCH}" == "armv7" ]; then
                    PLATFORM="linux/arm/v7"
                elif [ "${JOB_ARCH}" == "ppc64le" ]; then
                    PLATFORM="linux/ppc64le"
                elif [ "${JOB_ARCH}" == "x86" ]; then
                    PLATFORM="linux/386"
                elif [ "${JOB_ARCH}" == "x86_64" ]; then
                    PLATFORM="linux/amd64"
                fi

                docker build --platform ${PLATFORM} \
                    --build-arg DOCKER_ALPINE_VERSION=${ALPINE_VERSION} \
                    --build-arg BUILD_DATE=${BUILD_DATE} \
                    --build-arg ALPINE_ARCHITECTURE=${JOB_ARCH} \
                    --build-arg ALPINE_RELEASE=${ALPINE_RELEASE} \
                    --build-arg ALPINE_VERSION=${ALPINE_VERSION} \
                    --build-arg MAXMIND_GEOLITE_VERSION="${MAXMIND_GEOLITE_VERSION}" \
                    --build-arg MAXMIND_GEOLITE_VERSION_DATE="${MAXMIND_GEOLITE_VERSION_DATE}" \
                    --push \
                    -t ${GITLAB_CR_URL}/${GITLAB_CR_GROUP}/${GITLAB_CR_IMAGE}:test-${JOB_ARCH} \
                    -t ${GITLAB_CR_URL}/${GITLAB_CR_GROUP}/${GITLAB_CR_IMAGE}:test-${MAXMIND_GEOLITE_VERSION}-${JOB_ARCH} \
                    -f ./Dockerfile .

            done < "./.arch"
        - |
            echo "Push Manifest Test -> ${GITLAB_CR_URL}/${GITLAB_CR_GROUP}/${GITLAB_CR_IMAGE}:test"

            docker manifest create ${GITLAB_CR_URL}/${GITLAB_CR_GROUP}/${GITLAB_CR_IMAGE}:test \
                --amend ${GITLAB_CR_URL}/${GITLAB_CR_GROUP}/${GITLAB_CR_IMAGE}:test-aarch64 \
                --amend ${GITLAB_CR_URL}/${GITLAB_CR_GROUP}/${GITLAB_CR_IMAGE}:test-x86_64

            docker manifest push ${GITLAB_CR_URL}/${GITLAB_CR_GROUP}/${GITLAB_CR_IMAGE}:test

            echo "Push Manifest Test -> ${GITLAB_CR_URL}/${GITLAB_CR_GROUP}/${GITLAB_CR_IMAGE}:test-${MAXMIND_GEOLITE_VERSION}"

            docker manifest create ${GITLAB_CR_URL}/${GITLAB_CR_GROUP}/${GITLAB_CR_IMAGE}:test-${MAXMIND_GEOLITE_VERSION} \
                --amend ${GITLAB_CR_URL}/${GITLAB_CR_GROUP}/${GITLAB_CR_IMAGE}:test-${MAXMIND_GEOLITE_VERSION}-aarch64 \
                --amend ${GITLAB_CR_URL}/${GITLAB_CR_GROUP}/${GITLAB_CR_IMAGE}:test-${MAXMIND_GEOLITE_VERSION}-x86_64

            docker manifest push ${GITLAB_CR_URL}/${GITLAB_CR_GROUP}/${GITLAB_CR_IMAGE}:test-${MAXMIND_GEOLITE_VERSION}

container_test:
    stage: test
    image: docker:23.0.4
    allow_failure: true
    needs: ["build"]
    only:
        - master
    tags:
        - dind64
    script:
        - apk add --no-cache curl
        - |
          curl -LO https://storage.googleapis.com/container-structure-test/latest/container-structure-test-linux-amd64
          chmod +x container-structure-test-linux-amd64
          mv container-structure-test-linux-amd64 /usr/local/bin/container-structure-test
        - |
          docker pull $DOCKER_IMAGE_TEST
          container-structure-test -c test.yaml --image $DOCKER_IMAGE_TEST --test-report junit-test-result.xml --output junit test
    artifacts:         
        when: always
        paths:
            - $CI_PROJECT_DIR/junit-test-result.xml
        reports:
            junit:
                - $CI_PROJECT_DIR/junit-test-result.xml

container_scanning:
    stage: scanning
    allow_failure: true
    rules:
        - if: '$CI_COMMIT_REF_NAME == "master"'
          when: always
        - when: never
    needs: ["container_test"]
    before_script: []
    tags:
        - dind64
    variables:
        CS_IMAGE: $DOCKER_IMAGE_TEST

build_current:
    stage: build
    image: docker:23.0.4
    needs: ["download","build","container_test"]
    tags:
        - dind64
    dependencies:
        - download
    script:
        - |
            source maxmind
            cp db.mmdb $CI_PROJECT_DIR/src/db.mmdb
        - BUILD_DATE=$(date +"%Y-%m-%d")
        - echo "$DOCKER_HUB_PASSWORD" | docker login -u "$DOCKER_HUB_USERNAME" --password-stdin
        - |
            while IFS=" " read -r JOB_ARCH
            do
                PLATFORM="linux/amd64"
                if [ "${JOB_ARCH}" == "aarch64" ]; then
                    PLATFORM="linux/arm64"
                elif [ "${JOB_ARCH}" == "armhf" ]; then
                    PLATFORM="linux/arm/v6"
                elif [ "${JOB_ARCH}" == "armv7" ]; then
                    PLATFORM="linux/arm/v7"
                elif [ "${JOB_ARCH}" == "ppc64le" ]; then
                    PLATFORM="linux/ppc64le"
                elif [ "${JOB_ARCH}" == "x86" ]; then
                    PLATFORM="linux/386"
                elif [ "${JOB_ARCH}" == "x86_64" ]; then
                    PLATFORM="linux/amd64"
                fi

                docker build --platform ${PLATFORM} \
                    --build-arg DOCKER_ALPINE_VERSION=${ALPINE_VERSION} \
                    --build-arg BUILD_DATE=${BUILD_DATE} \
                    --build-arg ALPINE_ARCHITECTURE=${JOB_ARCH} \
                    --build-arg ALPINE_RELEASE=${ALPINE_RELEASE} \
                    --build-arg ALPINE_VERSION=${ALPINE_VERSION} \
                    --build-arg MAXMIND_GEOLITE_VERSION="${MAXMIND_GEOLITE_VERSION}" \
                    --build-arg MAXMIND_GEOLITE_VERSION_DATE="${MAXMIND_GEOLITE_VERSION_DATE}" \
                    --push \
                    -t ${DOCKER_HUB_GROUP}/${DOCKER_HUB_IMAGE}:${JOB_ARCH} \
                    -t ${DOCKER_HUB_GROUP}/${DOCKER_HUB_IMAGE}:${MAXMIND_GEOLITE_VERSION}-${JOB_ARCH} \
                    -f ./Dockerfile .

            done < "./.arch"
        - |
            echo "Push Manifest Latest -> ${DOCKER_HUB_GROUP}/${DOCKER_HUB_IMAGE}:latest"

            docker manifest create ${DOCKER_HUB_GROUP}/${DOCKER_HUB_IMAGE}:latest \
                --amend ${DOCKER_HUB_GROUP}/${DOCKER_HUB_IMAGE}:aarch64 \
                --amend ${DOCKER_HUB_GROUP}/${DOCKER_HUB_IMAGE}:x86_64

            docker manifest push ${DOCKER_HUB_GROUP}/${DOCKER_HUB_IMAGE}:latest

            echo "Push Manifest Current -> ${DOCKER_HUB_GROUP}/${DOCKER_HUB_IMAGE}:${MAXMIND_GEOLITE_VERSION}"

            docker manifest create ${DOCKER_HUB_GROUP}/${DOCKER_HUB_IMAGE}:${MAXMIND_GEOLITE_VERSION} \
                --amend ${DOCKER_HUB_GROUP}/${DOCKER_HUB_IMAGE}:${MAXMIND_GEOLITE_VERSION}-aarch64 \
                --amend ${DOCKER_HUB_GROUP}/${DOCKER_HUB_IMAGE}:${MAXMIND_GEOLITE_VERSION}-x86_64

            docker manifest push ${DOCKER_HUB_GROUP}/${DOCKER_HUB_IMAGE}:${MAXMIND_GEOLITE_VERSION}

readme:
    stage: readme
    image: docker:23.0.4
    needs: ["download","build","container_test","build_current"]
    dependencies:
        - download
    allow_failure: true
    variables:
        DOCKER_USER: $DOCKER_HUB_USERNAME
        DOCKER_PASS: $DOCKER_HUB_PASSWORD
        PUSHRM_SHORT: "Docker MaxMind GeoLite 2 image running on Alpine Linux"
        PUSHRM_TARGET: docker.io/$DOCKER_HUB_USERNAME/$DOCKER_HUB_IMAGE
        PUSHRM_DEBUG: 1
        PUSHRM_FILE: /tmp/repository/README.md
    tags:
        - dind64
    script:
        - source maxmind
        - export LAST_UPDATE=$(date +"%d.%m.%Y %H:%M:%S")
        - |
            git config --global user.email "mauro.cardillo@gmail.com"
            git config --global user.name "Mauro Cardillo"
            git clone https://gitlab-ci-token:${CI_JOB_TOKEN}@${CI_SERVER_HOST}/$CI_PROJECT_PATH.git /tmp/repository
        - cd /tmp/repository
        - echo "Generate README for MaxMind GeoLite 2 Version ${MAXMIND_GEOLITE_VERSION}"
        - |
            sed "s/\%ALPINE_RELEASE\%/${ALPINE_RELEASE}/g" README.tpl > README_COMPILE.md1
            sed "s/\%ALPINE_VERSION\%/${ALPINE_VERSION}/g" README_COMPILE.md1 > README_COMPILE.md2
            sed "s/\%MAXMIND_GEOLITE_VERSION\%/${MAXMIND_GEOLITE_VERSION}/g" README_COMPILE.md2 > README_COMPILE.md3
            sed "s/\%MAXMIND_GEOLITE_VERSION_DATE\%/${MAXMIND_GEOLITE_VERSION_DATE}/g" README_COMPILE.md3 > README_COMPILE.md4
            sed "s/\%LAST_UPDATE\%/${LAST_UPDATE}/g" README_COMPILE.md4 > README.md
            rm -rf README_COMPILE.*
        - |
            git add .
            git commit -m "Update README for MaxMind GeoLite 2 Version ${MAXMIND_GEOLITE_VERSION}"
            git push --push-option="ci.skip" https://gitlab-ci-token:${GITLAB_TOKEN}@${CI_SERVER_HOST}/$CI_PROJECT_PATH.git HEAD:$CI_COMMIT_BRANCH
        - |
            curl -LO https://github.com/christian-korneck/docker-pushrm/releases/download/v1.9.0/docker-pushrm_linux_amd64 
            chmod +x docker-pushrm_linux_amd64
            mv docker-pushrm_linux_amd64 /usr/local/bin/docker-pushrm
        - |
            docker-pushrm docker.io/$DOCKER_HUB_USERNAME/$DOCKER_HUB_IMAGE

clean:
    stage: clean
    allow_failure: true
    tags:
        - dind64
    script:
        - |
            docker images ${GITLAB_CR_URL}/${GITLAB_CR_GROUP}/${GITLAB_CR_IMAGE} -q | xargs --no-run-if-empty docker rmi -f
            docker images ${DOCKER_HUB_GROUP}/${DOCKER_HUB_IMAGE} -q | xargs --no-run-if-empty docker rmi -f
            docker volume ls -qf dangling=true | xargs --no-run-if-empty docker volume rm
